package com.example.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction().add(R.id.screen_fragment, new MainScreenFragment()).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.button_fragment, new BottomPanelFragment()).commit();
    }
}
